import os
import time
from flask import Flask
from flask_restx import Api
from flask_cors import CORS

from config.config import DatabaseConfig, Config

try:
    os.remove(":memory")
except:
    pass

Config.INIT_TIME = time.time()
DatabaseConfig.init_db()

from controller.root_controller import api as ns1
from controller.stats_controller import api as ns2
from service.data_pipeline_service import DataPipelineService

pipeline = DataPipelineService()
pipeline.load_data_and_populate_db()

app = Flask(__name__)
CORS(app)
api = Api(version='1.0', title='Chaos test tak API', doc='/api/doc/')
api.add_namespace(ns1, path='/api')
api.add_namespace(ns2, path='/api/stats')
api.init_app(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=os.environ.get('PORT', None) or 3000)
