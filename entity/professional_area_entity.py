from config.config import DatabaseConfig
from sqlalchemy import Column, Integer, String, UniqueConstraint


class ProfessionalArea(DatabaseConfig.Base):
    __tablename__ = "professional_area"

    id = Column(Integer, primary_key=True)
    professional_area_name = Column(String(255), nullable=False, unique=True, sqlite_on_conflict_unique='IGNORE')

    def __init__(self, professional_area_name):
        self.professional_area_name = professional_area_name

    def __repr__(self) -> str:
        return f"<Profession(id={self.id}, name={self.professional_area_name})>"
