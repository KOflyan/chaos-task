from config.config import DatabaseConfig
from sqlalchemy import Column, Integer, String, UniqueConstraint


class Organisation(DatabaseConfig.Base):
    __tablename__ = "organisation"

    id = Column(Integer, primary_key=True)
    organisation_name = Column(String(255), nullable=False, unique=True, sqlite_on_conflict_unique='IGNORE')

    def __init__(self, organisation_name):
        self.organisation_name = organisation_name

    def __repr__(self) -> str:
        return f"<Organisation(id={self.id}, name={self.organisation_name})>"
