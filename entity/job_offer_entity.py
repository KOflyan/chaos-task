from datetime import datetime

from config.config import DatabaseConfig
from sqlalchemy import Column, Integer, Numeric, \
    Date, ForeignKeyConstraint, String, Index, ForeignKey

from entity.organisation_entity import Organisation
from entity.professional_area_entity import ProfessionalArea


class JobOffer(DatabaseConfig.Base):
    __tablename__ = "job_offer"
    id = Column(Integer, primary_key=True)
    organisation_id = Column(Integer, ForeignKey(Organisation.id), nullable=False)
    professional_area_id = Column(Integer, ForeignKey(ProfessionalArea.id), nullable=False)
    task_description = Column(String(5000), nullable=False)
    offer_link = Column(String(500), nullable=False, unique=True, sqlite_on_conflict_unique='IGNORE')
    address = Column(String(500), nullable=False)
    offer_expires = Column(Date, nullable=False)
    lat = Column(Numeric, nullable=False)
    long = Column(Numeric, nullable=False)
    ForeignKeyConstraint(
        ('organisation_id',),
        ('organisation.id',),
        onupdate="CASCADE", ondelete="CASCADE",
        name="fk__job_offer__organisation"
    )
    ForeignKeyConstraint(
        ('professional_area_id',),
        ('professional_area.id',),
        onupdate="CASCADE", ondelete="CASCADE",
        name="fk__job_offer__profession"
    )
    Index("idx__job_offer__organisation_id", "organisation_id"),
    Index("idx__job_offer__profession_id", "professional_area_id")

    def __init__(self, organisation_id: int, professional_area_id: int, task_description: str, offer_link: str, address: str,
                 offer_expires: datetime, lat: float, long: float):
        self.organisation_id = organisation_id
        self.professional_area_id = professional_area_id
        self.task_description = task_description
        self.offer_link = offer_link
        self.address = address
        self.offer_expires = offer_expires
        self.lat = lat
        self.long = long

    def __repr__(self) -> str:
        return f"<Job Offer(id={self.id}, organisation_id={self.organisation_id}, professional_area_id={self.professional_area_id}, " \
               f"task={self.task_description})>"
