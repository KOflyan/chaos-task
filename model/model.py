class TotalDatabaseRecordsDto:

    def __init__(self, offers: int, organisations: int, professional_areas: int):
        self.offers = offers
        self.organisations = organisations
        self.professional_areas = professional_areas
