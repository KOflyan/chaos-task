class GenericException(RuntimeError):

    def __init__(self, message="UnknownError occurred"):
        super().__init__()
        self.message = message

    def __str__(self) -> str:
        return self.message


class DataFetchFailed(GenericException):
    def __init__(self, message="Could not fetch data; response format not supported"):
        super().__init__(message)

