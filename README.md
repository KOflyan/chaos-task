Chaos Test Task - API
====================

Prerequisites: 
* Python 3.6+
* virtualenv

Setting up the project
----------------------

1) Clone the repo

2) Set up virtual environment
    ```bash
    cd chaos-task
    virtualenv --python python3.6 venv
    source venv/bin/activate
    ```

3) Install deps
    ```bash
    pip3 install -r requirements.txt
    ```

4) Run
    ```bash
   python3 app.py 
   ```
   
   
Documentation
-------------

Swagger available at: http://localhost:3000/api/doc

Tools used:
* Flask
* SQLAlchemy (ORM tool)
* SQLite (in-memory)
* Flask-restx (swagger)

When you start the app, it fetches the data from http://gis.vantaa.fi/rest/tyopaikat/v1/kaikki , 
organises it a bit and persists to the database.
The api provides few endpoints for data aggregation.
The api follows MVC design pattern.