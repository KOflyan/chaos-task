import requests as rq
from typing import Any, Tuple
from datetime import datetime

from sqlalchemy.engine import Connection

from config.config import Config, DatabaseConfig
from entity.job_offer_entity import JobOffer
from entity.organisation_entity import Organisation
from entity.professional_area_entity import ProfessionalArea
from exception.exception import DataFetchFailed
from sqlalchemy import insert
from sqlalchemy.engine.result import ResultProxy


class DataPipelineService:

    def __init__(self):
        self.session = DatabaseConfig.session
        self.conn: Connection = self.session.connection()

    def load_data_and_populate_db(self) -> None:
        if self.conn.closed:
            self.conn = self.session.connection()

        data = self._load_data()
        self._populate_db(data)

    def _populate_db(self, data: list) -> None:
        for element in data:
            organisation, prof_area = self._get_or_persist_prof_area_and_organisation(
                element['organisaatio'], element['ammattiala'])

            offer = DataPipelineService._get_job_offer_instance(organisation.id, prof_area.id, element)
            self.session.add(offer)
            self.session.commit()

        self.conn.close()

    def _get_or_persist_prof_area_and_organisation(self, oname: str, paname: str) -> Tuple[Organisation,
                                                                                           ProfessionalArea]:
        o = self.session.query(Organisation).filter_by(organisation_name=oname).first()
        p = self.session.query(ProfessionalArea).filter_by(professional_area_name=paname).first()

        if o is None:
            o = Organisation(oname)
            self._add_entity(o)
        if p is None:
            p = ProfessionalArea(paname)
            self._add_entity(p)

        return o, p

    def _add_entity(self, e: Any) -> None:
        self.session.add(e)
        self.session.commit()

    @staticmethod
    def _get_job_offer_instance(oid: int, pid: int, element: dict) -> JobOffer:
        return JobOffer(
                oid,
                pid,
                element['tyotehtava'],
                element['tyoavain'],
                element['osoite'],
                datetime.strptime(element['haku_paattyy_pvm'], '%Y-%m-%d'),
                element['x'],
                element['y']
            )

    @staticmethod
    def _insert_and_return_id(table: Any, values: dict, conn: Connection) -> int:
        stmt = insert(table).values(values)
        r: ResultProxy = conn.execute(stmt)

        return r.inserted_primary_key[0]

    @staticmethod
    def _load_data() -> list:
        result = rq.get(Config.DATASET_URL).json()
        if not result or not isinstance(result, list):
            raise DataFetchFailed()
        return result

