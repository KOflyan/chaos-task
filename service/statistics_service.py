from typing import Any, List

from sqlalchemy import text
from sqlalchemy.orm import Session

from config.config import DatabaseConfig
from entity.job_offer_entity import JobOffer
from entity.organisation_entity import Organisation
from entity.professional_area_entity import ProfessionalArea
from model.model import TotalDatabaseRecordsDto
from query.query import COUNT_OFFERS_BY_ORGANISATIONS, COUNT_OFFERS_BY_PROFESSIONAL_AREAS, GET_ALL_OFFERS_WITH_DETAILS
from utils.utils import Utils


class StatisticsService:

    def __init__(self, session: Session = DatabaseConfig.session, engine: Any = DatabaseConfig.engine):
        self.session = session
        self.engine = engine

    def total(self) -> TotalDatabaseRecordsDto:
        return TotalDatabaseRecordsDto(
            self._count(JobOffer),
            self._count(Organisation),
            self._count(ProfessionalArea)
        )

    def count_offers_by_professional_areas(self) -> List[dict]:
        return self._fetch_data(COUNT_OFFERS_BY_PROFESSIONAL_AREAS)

    def count_offers_by_organisations(self) -> List[dict]:
        return self._fetch_data(COUNT_OFFERS_BY_ORGANISATIONS)

    def get_all_offers_with_details(self) -> List[dict]:
        return self._fetch_data(GET_ALL_OFFERS_WITH_DETAILS)

    def _fetch_data(self, query_string: str) -> List[dict]:
        with self.engine.connect() as conn:
            q = text(query_string)
            rs = conn.execute(q).fetchall()
        return Utils.serialize_result_set(rs)

    def _count(self, e: Any) -> int:
        with self.engine.connect() as conn:
            rs = conn.execute(f"SELECT COUNT(*) FROM {e.__tablename__}").scalar()
        return rs
