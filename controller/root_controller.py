import time
from flask_restx import Namespace, Resource

from config.config import Config

api = Namespace('Misc')


@api.route('/uptime')
class Uptime(Resource):

    def get(self):
        return {'uptime': time.time() - Config.INIT_TIME}
