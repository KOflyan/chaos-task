from typing import List

from flask import Blueprint
from flask_restx import Namespace, Resource

from service.statistics_service import StatisticsService

statistics = Blueprint('statistics', __name__)
service = StatisticsService()

api = Namespace('Statistics')


@api.route('/offers_by_profession_areas')
class OffersByProfessionAreas(Resource):
    def get(self) -> List[dict]:
        return service.count_offers_by_professional_areas()


@api.route('/offers_by_organisations')
class OffersByOrganisations(Resource):
    def get(self) -> List[dict]:
        return service.count_offers_by_organisations()


@api.route('/offers')
class Offers(Resource):
    def get(self) -> List[dict]:
        return service.get_all_offers_with_details()


@api.route('/total_records')
class TotalRecords(Resource):
    def get(self) -> dict:
        return service.total().__dict__
