from typing import List


class Utils:

    @staticmethod
    def serialize_result_set(rows) -> List[dict]:
        return [(dict(row.items())) for row in rows]
