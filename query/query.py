COUNT_OFFERS_BY_ORGANISATIONS = \
    "SELECT " \
        "org.id AS org_id, " \
        "org.organisation_name AS organisation, " \
        "COUNT(*) AS offers " \
    "FROM job_offer o " \
        "JOIN organisation org ON org.id = o.organisation_id " \
    "GROUP BY org.id " \
    "ORDER BY org.id"


COUNT_OFFERS_BY_PROFESSIONAL_AREAS = \
    "SELECT " \
        "p.id AS area_id, " \
        "p.professional_area_name AS area, " \
        "COUNT(*) AS offers " \
    "FROM job_offer o " \
        "JOIN professional_area p ON p.id = o.professional_area_id " \
    "GROUP BY p.id " \
    "ORDER BY p.id"

GET_ALL_OFFERS_WITH_DETAILS = \
    "SELECT " \
        "o.id, o.task_description, o.offer_link, o.address, o.offer_expires, o.lat, o.long, " \
        "p.professional_area_name AS area, org.organisation_name AS organisation " \
    "FROM job_offer o " \
        "JOIN professional_area p ON p.id = o.professional_area_id " \
        "JOIN organisation org ON org.id = o.organisation_id " \
    "ORDER BY o.id"
