from sqlalchemy import create_engine
from sqlalchemy.engine import Connection
from sqlalchemy.ext.declarative import declarative_base, DeclarativeMeta
from sqlalchemy.orm import Session


class Config:
    INIT_TIME = None
    DATASET_URL = "http://gis.vantaa.fi/rest/tyopaikat/v1/kaikki"
    DB_URI = "sqlite:///:memory"


class DatabaseConfig:
    Base = None
    engine = None
    session: Session = None
    conn: Connection = None

    @staticmethod
    def init_db() -> None:
        DatabaseConfig.engine = create_engine(Config.DB_URI, echo=True, encoding='utf-8')
        DatabaseConfig.Base = declarative_base(bind=DatabaseConfig.engine)

        # loads entities
        from entity import job_offer_entity, organisation_entity, professional_area_entity

        DatabaseConfig.Base.metadata.create_all(DatabaseConfig.engine)
        DatabaseConfig.session = DatabaseConfig.create_session()
        DatabaseConfig.conn = DatabaseConfig.session.connection()

    @staticmethod
    def create_session() -> Session:
        return Session(bind=DatabaseConfig.engine)
